# Command List

```
docker ps
```

# Start nginx

```
docker run --name some-nginx -p 80:80 -d nginx:alpine
```

# Open Browser

- http://localhost
- http://192.168.99.100

# list all machine

```
docker-machine ls
```

# execute shell on nginx

```
docker exec -it some-nginx sh
```

# stop and remove container

```
docker stop some-nginx
docker rm some-nginx
```

# start new server with volume

```
docker run --name some-nginx -p 80:80 -d -v /Volumes/KDrive/train/2019_09/devops/html:/usr/share/nginx/html nginx:alpine
```

# inspect

```
docker inspect some-nginx
```

# create mysql container

```
docker run --name mysql-svr -p 3306:3306 -d -v /Volumes/KDrive/train/2019_09/devops/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=root1234 -e MYSQL_DATABASE=simpledb -e MYSQL_USER=myuser -e MYSQL_PASSWORD=mypassword mysql:5.7
```

# test mysql

```
docker exec -it mysql-svr bash
mysql -uroot -p
use simpledb
show tables;
exit
exit
```

# test stop / start container

```
docker stop some-nginx
docker ps
docker ps -a
docker start some-nginx
docker exec -it mysql-svr mysql -uroot -p
```

# view log

```
docker logs some-nginx
docker logs -f some-nginx
```

# workshop test network

```
docker stop some-nginx
docker stop mysql-svr
docker rm some-nginx
docker rm mysql-svr
docker network create my-net
docker run --name some-nginx -p 80:80 -d -v /Volumes/KDrive/train/2019_09/devops/html:/usr/share/nginx/html --network=my-net nginx:alpine

docker run --name mysql-svr -p 3306:3306 -d -v /Volumes/KDrive/train/2019_09/devops/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=root1234 -e MYSQL_DATABASE=simpledb -e MYSQL_USER=myuser -e MYSQL_PASSWORD=mypassword mysql:5.7

docker exec -it some-nginx sh
ping mysql-svr

docker stop mysql-svr
docker rm mysql-svr

docker run --name mysql-svr -p 3306:3306 -d -v /Volumes/KDrive/train/2019_09/devops/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=root1234 -e MYSQL_DATABASE=simpledb -e MYSQL_USER=myuser -e MYSQL_PASSWORD=mypassword --network=my-net mysql:5.7

docker exec -it some-nginx sh
ping mysql-svr
```

# build image from Dockerfile

```
docker build -t my-nginx:latest .
docker image ls

docker run -p 81:80 -d --name some-my my-nginx
```

# step push image gitlab registry

```
docker login registry.gitlab.com
docker tag my-nginx:latest registry.gitlab.com/sommai.k/devops:latest
docker images
docker push registry.gitlab.com/sommai.k/devops
```

# tag image

```
docker tag my-nginx:latest registry.gitlab.com/sommai.k/devops:1

docker build -t registry.gitlab.com/sommai.k/devops:2 .

docker tag registry.gitlab.com/sommai.k/devops:2 registry.gitlab.com/sommai.k/devops:latest

docker push registry.gitlab.com/sommai.k/devops
```

# test run from custom image

```
docker run -p 81:80 -d --name some-my registry.gitlab.com/sommai.k/devops:2

docker run -p 82:80 -d --name some-my-v1 registry.gitlab.com/sommai.k/devops:1
```

# test compose

```
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker-compose up -d
docker-compose ps
```

# create mgr1

```
docker-machine create mgr1
docker-machine ls
eval $(docker-machine env mgr1)
docker run -p 80:80 -d --name some-mgr1 registry.gitlab.com/sommai.k/devops
```

# create node1, node2

```
docker-machine create node1
docker-machine create node2
```

# create swarm

```
docker-machine ssh mgr1
docker-machine ip mgr1
docker swarm init --advertise-addr=192.168.99.137

docker login registry.gitlab.com

docker service create --with-registry-auth --replicas 1 --name my-nginx --constraint "node.role != manager" --publish 80:80 registry.gitlab.com/sommai.k/devops

docker service rm my-nginx

docker service update my-nginx --image registry.gitlab.com/sommai.k/devops:1
```

# create service grafana

```
git clone https://github.com/stefanprodan/swarmprom.git

cd swarmprom

docker stack deploy -c docker-compose.yml mon
```

# create jenkins

```
docker-machine create jenkins

docker-machine ssh jenkins

docker run -p 8080:8080 -p 50000:50000 \
-e JNLP_PROTOCOL_OPTS=-Dorg.jenkinsci.remoting.engine.JnlpProtocol3.disabled=false \
-v /var/run/docker.sock:/var/run/docker.sock:ro \
-v /mnt/sda1/jenkins:/var/jenkins_home -d --user root jenkins/jenkins:lts

docker ps

docker logs <<jenkin_id>>
```

# digital ocean

```
docker-machine create --driver digitalocean --digitalocean-access-token=<<token>> --digitalocean-region=sgp1 <<vm-name>>
```

# create agent at mgr1

```
docker-machine ssh mgr1
docker run -v /var/run/docker.sock:/var/run/docker.sock:ro \
-d \
--name mgr1 \
--user root \
korekontrol/docker-jnlp-slave-docker \
-url http://192.168.99.140:8080 \
a69b8aa3f7d05c66e7cade0f5c25ffc128c7c2abfa3c41c7d21d50f926330e12 mgr1
```

# workshop 1

```
git clone https://github.com/spring-projects/spring-petclinic
cd spring-petclinic

git remote set-url origin https://gitlab.com/<<YOUR_REPOSITORY>>/spring-petclinic.git

git push
```

# nexus

```
docker-machine create nexus-server
docker-machine ls
docker-machine ssh nexus-server
docker run -d -p 8081:8081 --name nexus sonatype/nexus3
docker logs -f nexus
docker exec -it nexus cat /opt/sonatype/sonatype-work/nexus3/admin.password
exit
```

# Demo and demo api

```
git clone https://github.com/Sommaik/demo

git clone https://github.com/Sommaik/demoAPI
```
